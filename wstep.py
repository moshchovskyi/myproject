class MyClass:
    def method1(self):
        print("Metoda 1")

    def method2(self, a_string):
        print(f"{a_string}")

c = MyClass()

# c.method1()
# c.method2("test")

class MyClass:
    def method1(self):
        print("Metoda 1 klasy MyClass")

class ChildClass(MyClass):
    def method2(self):
        print("Metoda 2 klasy ChildClass")

    def method1(self):
        MyClass.method1(self)
        print("Metoda 1 klasy ChildClass")

a = ChildClass()
a.method1()


class Person:
    pass
class Employee(Person):
    pass
class Manager(Employee):
    pass

class Animal:
    pass

class Enterpreneur(Person, Animal):
    pass
