def add(x, y):
    """Add function"""
    return x+y

def substract(x, y):
    """Substract function"""
    return x-y

def multiply(x, y):
    """Multiply function"""
    return x*y

def divide(x, y):
    """Divide function"""
    if y==0:
        raise ValueError("Cannot devide by 0")
    return x/y
