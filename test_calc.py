import unittest

import calc

class TestAdd(unittest.TestCase):
    """
    Test the add function from the calc library
    """

    def test_add_integer(self):
        """
        Test that the addition of two integers returns the correct
        """
        result = calc.add(1, 2)
        self.assertEqual(result, 3)

    def test_add_float(self):
        """
        Test that the addition of two floats returns the coorect
        """
        result = calc.add(10.5, 2)
        self.assertAlmostEqual(result, 12.5)

    def test_add_strings(self):
        """
        Test the addition of two integers the two strings
        concateneted string
        """
        result = calc.add('abc', 'def')
        self.assertEqual(result, 'abcdef')

if __name__ == "__main__":
    unittest.main()


