import pytest
import unittest

import math

from func import circle_area

def test_area():
    assert circle_area(1) == math.pi
    assert  circle_area(0) == 0
    assert circle_area(2.1) == math.pi * (2.1*2)

test_area()

def test_values():
    with pytest.raises(ValueError):
        circle_area(-2)

test_values()

def test_types():
    with pytest.raises(TypeError):
        circle_area("radius")

test_types()

